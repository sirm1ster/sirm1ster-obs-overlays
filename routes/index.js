var express = require('express');
var router = express.Router();
const api = require('../services/api/api');
const { camelize } = require('../services/utils');

const config = require('../configuration/config.json');

for (let i = 0; i < config.pages.length; i++) {
  const page = config.pages[i];
  router.get(page.route, function(req, res, next) {
    if(page.type == 'api') {

      const apiService = page.route.substring(page.route.lastIndexOf('/') + 1, page.route.indexOf('?'));
      const serviceCamelized = camelize(apiService);
      const filePath = req.query.file;
      const username = req.query.username;

      if(filePath && username) {
        try {
          const apiResponse = api[serviceCamelized](filePath, username);
          res.status(200).json(apiResponse);
        } catch (error) {
          res.status(500).json({"status": 500, "message": "Error"});
        }
      }

    } else {
      res.render(page.template, { "title": page.title });
    }
  });

}

module.exports = router;
