const _toggleStatusBanner = (data) => {
  const statusBanner = document.getElementById(`status-brb`);

  if(!statusBanner) return;

  if(statusBanner.classList.value.indexOf('active') === -1) {
    statusBanner.classList.add('active');
  } else {
    statusBanner.classList.remove('active');
  }
}

const _updateCounter = (data) => {
  if(!document.getElementById('counter-template')) return;

  const templateCounter = document.getElementById('counter-template').innerHTML;
  const templateCounterScript = Handlebars.compile(templateCounter);
  for (const key in data) {
    if (data.hasOwnProperty(key)) {
      const value = data[key];
      document.getElementById(`${key}-count`).innerHTML = templateCounterScript(value.count);
    }
  }
}
const _updateLegoBuild = (data) => {
  const templateBuildNr = document.getElementById('legobuildnr-template').innerHTML;
  const templateBuildImg = document.getElementById('legobuildimage-template').innerHTML;
  console.log(templateBuildImg)
  const templateBuildNrScript = Handlebars.compile(templateBuildNr);
  const templateBuildImgScript = Handlebars.compile(templateBuildImg);

  document.getElementById(`legobuildnr`).innerHTML = templateBuildNrScript(data.id);
  document.getElementById(`legobuildimage`).innerHTML = templateBuildImgScript(data.image);
}

const _cheerMessage = (data) => {
  const cheerMessageTemplate = document.getElementById('cheermessage-template');
  if(!cheerMessageTemplate) return;

  const cheerMessageBanner = document.getElementById(`cheer-message`);
  const templateCheerMessageScript = Handlebars.compile(cheerMessageTemplate.innerHTML);

  cheerMessageBanner.innerHTML = templateCheerMessageScript(data);

  cheerMessageBanner.classList.toggle('active');
  setTimeout(() => {
    cheerMessageBanner.classList.toggle('active');
  }, 16000);
}

const _updateScoreboard = (data) => {
  console.log(data);
  const templateScoreboardCrew = document.getElementById('scoreboard-crew-template').innerHTML;
  const templateScoreboardPlayer = document.getElementById('scoreboard-player-template').innerHTML;
  const templateScoreboardCrewScript = Handlebars.compile(templateScoreboardCrew);
  const templateScoreboardPlayerScript = Handlebars.compile(templateScoreboardPlayer);

  let index = 0
  for (const [key, value] of Object.entries(data)) {
    index++;
    console.log(value);
    document.getElementById(`crew-${index}`).innerHTML = templateScoreboardCrewScript(value);
    document.getElementById(`crew-${index}-player`).innerHTML = templateScoreboardPlayerScript(value.players);
  }

}

const startCountdown = (duration, display) => {
  let timer = duration, minutes, seconds;
  const timerInterval =  setInterval(() => {
    minutes = parseInt(timer / 60, 10);
    seconds = parseInt(timer % 60, 10);

    seconds = seconds < 10 ? "0" + seconds : seconds;

    display.textContent = `${minutes}:${seconds}`;

    if(--timer < 0) {
      clearInterval(timerInterval);
    }
  }, 1000);
}

const _snapMessage = () => {
  const snapAlertBanner = document.getElementById(`snap-banner`);

  snapAlertBanner.classList.add('active');
  setTimeout(() => {
    snapAlertBanner.classList.remove('active');
  }, 4000);
}

document.addEventListener("DOMContentLoaded", () => {
  const url = 'ws://localhost:8080';
  const ws = new WebSocket(url);

  ws.onmessage = (response) => {
    const jsonData = JSON.parse(response.data);

    if(jsonData.type === 'crewbattle') {
      if(jsonData.data.crews)
        _updateScoreboard(jsonData.data.crews);
    }

    if(jsonData.type === 'initialize') {
      if(jsonData.data)
        _updateCounter(jsonData.data.counters);
    }

    if(jsonData.type === 'counter') {
      if(jsonData.data.counters)
        _updateCounter(jsonData.data.counters);
    }

    if(jsonData.type === 'lego') {
      if(jsonData.data)
        _updateLegoBuild(jsonData.data);
    }

    if(jsonData.type === 'overlay') {
      if(jsonData.data)
        _toggleStatusBanner(jsonData.data);
    }

    if(jsonData.type === 'cheer') {
      if (jsonData.data) {
        _cheerMessage(jsonData.data);
      }
    }

    if(jsonData.type === 'snap') {
      if (jsonData.data) {
        _snapMessage(jsonData.data);
      }
    }

    const timerCountdownEls = document.getElementsByClassName(`countdown`);
    if(timerCountdownEls) {
      for (let i = 0; i < timerCountdownEls.length; i++) {
        const countdownEl = timerCountdownEls[i];
        const dataDuration = countdownEl.getAttribute('data-time') * 60;
        startCountdown(dataDuration, countdownEl);
      }
    }
  }
});