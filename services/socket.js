const WebSocket = require('ws');
const { log, debug } = require('./utils');

let websocket = '';

const connect = (callback) => {
  debug('Establishing websocket');

  wss = new WebSocket.Server({port: 8080});
  wss.on('connection', ws => {
    log(`Websocket connection established successfully.`);
    websocket = ws;
    if(callback) callback();
  });
}

const send = (data) => {
  debug('Sending data to websocket');

  websocket.send(data);
}

module.exports = {
  connect: connect,
  send: send
};