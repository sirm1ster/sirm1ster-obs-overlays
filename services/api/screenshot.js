const screenshot = () => {
  return {"filename": "screenshot.png"};
}

module.exports = {
  screenshot
}