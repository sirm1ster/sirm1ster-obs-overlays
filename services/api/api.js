const { Webhook } = require('discord-webhook-node');

const config = require('../../configuration/config.json');
const socket = require('../socket');

const imageToDiscord = (filePath, username) => {

  const hook = new Webhook(config.discord.webhook);

  const snapResponseObject = {
    type: 'snap',
    data: {
      user: username
    },
  }

  try {
    console.dir('Sending Websocket data...');

    socket.send(JSON.stringify(snapResponseObject));
  } catch (error) {
    console.error(error);
  }

  hook.setUsername(config.discord.bot_name);
  hook.setAvatar(config.discord.bot_avatar);

  hook.send(`${username} created a new screenshot:`);
  hook.sendFile(filePath);

  return {"status": 200, "message": "File sent successfully"};
}

module.exports = {
  imageToDiscord
}