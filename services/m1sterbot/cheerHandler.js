const messages = require("../messages");
const socket = require("../socket");
const { truncate } = require("../utils");

// Rupees with their minimum amount of bits required
const bitsRupees = {
  1: 'green',
  25: 'blue',
  50: 'red',
  100: 'purple',
  500: 'silver',
  1000: 'gold'
}

const cleanMessage = (message) => {
  const regex = new RegExp(/(\bcheer|\bbiblethumb|\bcheerwhal|\bcorgo|\buni|\bshowlove|\bparty|\bseemsgood|\bpride|\bkappa|\bfrankerz|\bheyguys|\bdansgame|\belegiggle|\btrihard|\bfailfish|\bnotlikethis|\bswiftrage|\b4head|\bkreygasm|\bvohiyo|\bpjsalt|\bmrdestructoid|\bbday|\bripcheer|\bshamrock)\d+/, 'gi');
  const newString = message.replace(regex, '');

  console.log(newString.replace(/\s\s+/g, ' '));

  return truncate(newString.replace(/\s\s+/g, ' ').trim(), 160, true);
}

const cheerHandler = (target, userstate, message) => {
  let responseObj = {
    target: target,
    msg: "",
  };

  const rupeeColor = (bits) => {
    for (const key in bitsRupees) {
      if(Number(key) >= Number(bits)) {
        return bitsRupees[key];
      }
    }
  }

  const cheerResponseObject = {
    type: 'cheer',
    data: {
      bits: userstate.bits,
      rupee: rupeeColor(userstate.bits),
      user: userstate["display-name"],
      message: cleanMessage(message)
    },
  }

  socket.send(
    JSON.stringify(cheerResponseObject)
  );

  responseObj.msg = messages(cheerResponseObject.type, cheerResponseObject.data);

  return responseObj;
}

module.exports = {
  cheerHandler: cheerHandler
};

