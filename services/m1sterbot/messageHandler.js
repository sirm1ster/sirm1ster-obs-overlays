const { streamInfo } = require("./connectHandler");

const messages = require("../messages");
const { getGameConfig } = require("../game");
const { debug, timestamp, chatTimestamp, isModerator, convertChatMessage, validChatCommand } = require("../utils");
const { counter, quotes, status } = require("../commands");
const { crewbattle, crewStocks, crewPlayerChange } = require("../commands/crewbattle");
const config = require("../../configuration/config.json");

// Called every time a message comes in
const messageHandler = (target, context, message, self) => {

  debug(`streamInfo: ${streamInfo}`);

  const messageObject = convertChatMessage(message);

  if(!validChatCommand(messageObject.command)) {
    return false;
  }

  debug(`Command triggered: ${JSON.stringify(messageObject.command)}`);

  // If the command is known, let's execute it
  if (config.twitch.commands[messageObject.command]) {
    debug(`Command supported: ${JSON.stringify(messageObject.command)}`);

    if (streamInfo === (null || undefined)) return;

    let responseObj = {
      target: target,
      msg: "",
    };

    /**
     * COMMAND: !lego
     */
    if(messageObject.command === '!lego') {
      // const legoResponse = legos(messageObject, streamInfo);
    }

    /**
     * COMMAND: !dadjoke
     */
    if(messageObject.command === '!dadjoke') {
      const quoteType = 'dadjoke';
      const dadJokeResponse = quotes(messageObject, streamInfo.game_name, quoteType);

      responseObj.msg = _handleChatResponse(quoteType, dadJokeResponse);
    }

    /** START: MODERATOR COMMANDS */
    debug(`>>> isMODERATOR: ${isModerator(messageObject, context)}`);
    if(isModerator(messageObject, context)) {

      debug(`Entering MOD/BROADCASTER mode`);

      /**
       * COMMAND: ?brb
       */
      if(messageObject.command === "?brb") {
        const statusType = 'status';
        const statusResponse = status('brb');

        if(!statusResponse) return;

        responseObj.msg = _handleChatResponse(statusType, statusResponse);
      }

      /**
       * COMMAND: ?lego [buildnr] [name] [pieces] [url] [instructions] [image]
       */
      if (messageObject.command === "?lego") {
        const counterType = 'bags';
        const legoResponse = legos(messageObject);

        responseObj.msg = _handleChatResponse(counterType, legoResponse, legoResponse.meta.modifier);
      }

      /**
      * COMMAND: ?rip
      */
      if (messageObject.command === "?rip") {
        const counterType = 'rip';
        const gameConfig = getGameConfig(streamInfo.game_id);

        debug(`Fetched gameConfig: ${JSON.stringify(gameConfig)}`);

        if (!gameConfig) {
          return;
        }

        const ripCounterResponse = counter(counterType, gameConfig, messageObject.modifier);

        if(!ripCounterResponse) return;

        responseObj.msg = _handleChatResponse(counterType, ripCounterResponse, ripCounterResponse.meta.modifier);

      }

      /**
      * COMMAND: ?korok
      */
      if(messageObject.command === "?korok") {
        const counterType = 'korok';
        const gameConfig = getGameConfig(streamInfo.game_id);

        debug(`Fetched gameConfig: ${JSON.stringify(gameConfig)}`);

        if (!gameConfig) {
          return;
        }

        const korokCounterResponse = counter(counterType, gameConfig, messageObject.modifier);

        if(!korokCounterResponse) return;

        responseObj.msg = _handleChatResponse(counterType, korokCounterResponse, korokCounterResponse.meta.modifier);
      }

      /**
      * COMMAND: ?dadjoke
      */
      if(messageObject.command === "?dadjoke") {
        const quoteType = 'dadjoke';
        const dadJokeResponse = quotes(messageObject, streamInfo.game_name, quoteType);

        responseObj.msg = _handleChatResponse(quoteType, dadJokeResponse, dadJokeResponse.meta.modifier);
      }

      /**
      * CREW BATTLE COMMANDS
      *
      * COMMAND: ?crew
      */
      if(messageObject.command === "?crew") {
        const crewBattleResponse = crewbattle(messageObject);

        responseObj.msg = _handleChatResponse('crew', crewBattleResponse);
      }
      if(messageObject.command === "?seifcrew" || messageObject.command === "?sircrew") {
        let crewResponse;
        if(messageObject.modifier.substring(1) === 'stock') {
          crewResponse = crewStocks(messageObject, messageObject.command, messageObject.modifier);
        }
        if(messageObject.modifier === 'next' || messageObject.modifier === 'prev') {
          crewResponse = crewPlayerChange(messageObject, messageObject.command, messageObject.modifier);
        }
        if(messageObject.modifier.substring(1) === 'score') {
          crewResponse = crewStocks(messageObject, messageObject.command, messageObject.modifier);
        }

        responseObj.msg = _handleChatResponse('crew', crewResponse, crewResponse.meta.modifier);
      }
    };
    /** END: MODERATOR COMMANDS */

    debug(`[${timestamp()}] Executed command: ${messageObject.command}`);

    return {
      target: target,
      msg: responseObj.msg,
    };
  } else {
    // console.log(`* Unknown command '${msg}'`);
  }
};

const _handleChatResponse = (cmdType, cmdResponse, cmdModifier = '') => {
  debug(`=== _handleChatResponse ===`);

  debug(`Constructing chat message for '${JSON.stringify(cmdResponse)}'`);
  debug(`Modifying data with ${JSON.stringify(cmdModifier)} from ${JSON.stringify(cmdResponse)}`);

  const chatResponse = {
    id: cmdResponse.id,
    text: cmdResponse.text,
    category: cmdResponse.category,
    timestamp: chatTimestamp(cmdResponse.timestamp)
  };

  debug(`chatResponse: ${JSON.stringify(chatResponse)}`);

  let responseMsg = {};
  try {
    responseMsg = messages(cmdType, chatResponse, cmdResponse, cmdModifier);
  } catch (error) {
    console.log("Some error has occured.");
  }
  return responseMsg;
}

module.exports = {
  messageHandler
};