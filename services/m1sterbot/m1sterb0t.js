const tmi = require('tmi.js');

const appConfig = require('../../configuration/config.json');
const { connectHandler } = require("./connectHandler");
const { messageHandler } = require("./messageHandler");
const { cheerHandler } = require("./cheerHandler");
const { debug, log } = require("../utils");

const twitchOpts = {
  options: { debug: true },
  identity: {
    username: appConfig.twitch.bot_username,
    password: appConfig.twitch.oauth_token
  },
  connection: {
    reconnect: true,
    secure: true,
    server: process.env.TWITCHSERVER
  },
  channels: [
    appConfig.twitch.channel
  ]
};

const initialize = () => {
  // Create a client with our options
  const client = new tmi.Client(twitchOpts);

  // // Connect to Twitch:
  client.connect().catch(console.log);
    // .then(() => {
    //   setTimeout(() => {
    //     client.say(appConfig.twitch.channel, 'bits --username sirM1ster --bitscount 100 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget elit eu nunc eleifend varius quis nec elit. Vivamus sit amet diam cursus, placerat nibh non, sodales nulla. Duis ipsum lorem, ultricies eget sem id, pulvinar euismod mi. Nam tempus malesuada nulla, sit amet condimentum metus rhoncus id. Suspendisse potenti. Aliquam ac leo eget sapien laoreet congue eu at nulla.');
    //   }, 5000);
    // });
  client.on('connected', connectHandler);

  // // Register our event handlers (defined below)
  client.on('chat', (channel, userstate, message, self) => {
    if (self) return; // Ignore messages from the bot itself

    const chatMsg = messageHandler(channel, userstate, message, self);

    if(chatMsg) {
      sendMessage(chatMsg, client);
    }
  });

  client.on('cheer', (channel, userstate, message) => {
    debug(`channel: ${JSON.stringify(channel)}`);
    debug(`userstate: ${JSON.stringify(userstate)}`);
    debug(`message: ${JSON.stringify(message)}`);

    const chatMsg = cheerHandler(channel, userstate, message);

    if(chatMsg) {
      sendMessage(chatMsg, client);
    }
  });
}

const sendMessage = (mssg, client) => {
  debug(`Silencing M1STERB0T due to DEVELOP mode`);
  if(mssg && process.env.APPMODE !== 'dev') {
    client.say(mssg.target, mssg.msg);
  } else {
    log(`CHAT: (${mssg.target}) "${mssg.msg}"`);
  }
}

module.exports = {
  initialize: initialize,
  sendMessage: sendMessage
};