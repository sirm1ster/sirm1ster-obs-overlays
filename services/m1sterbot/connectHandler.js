const TwitchApi = require("node-twitch").default;

const socket = require("../socket");
const { getGameConfig } = require("../game");
const config = require("../../configuration/config.json");
const { debug, timestamp, log } = require("../utils");

const twitch = new TwitchApi({
  client_id: config.twitch.client_id,
  client_secret: config.twitch.client_secret,
});

let streamInfo = {};

// Called every time the bot connects to Twitch chat
const connectHandler = (addr, port) => {
  socket.connect(() => {
    if (!!streamInfo === false) {
      _getStreamInfo().then((res) => {
        streamInfo = res.data[0];

        if(!!streamInfo) {
          log(`🔴 ${config.twitch.channel.toUpperCase()} is LIVE.`);
          debug(`[${timestamp()}] ${JSON.stringify(res)}`);

          const liveStreamData = {};

          // Check if this is a "brickbuilding" (LEGO) stream
          // if(streamInfo.tag_ids.includes(config.twitch.tags.brickbuilding)) {
          //   debug(`🧱 LET'S GO LEGO!!!`);
          // } else {
          //   liveStreamData = getGameConfig(streamInfo.game_id);
          // }

          socket.send(
            JSON.stringify({
              type: 'initialize',
              data: liveStreamData
            })
          )
        } else {
          log(`[${timestamp()}] ⚪️ ${config.twitch.channel.toUpperCase()} is OFFLINE.`);
        }
      });
    } else {
      const liveStreamData = getGameConfig(streamInfo.game_id);
      socket.send(
        JSON.stringify({
          type: 'initialize',
          data: liveStreamData
        })
      )
    }
  });
};

const _getStreamInfo = async () => {
  const _channelName = config.twitch.channel;
  const stream = await twitch.getStreams({ channel: _channelName });
  return stream;
};

module.exports = {
  connectHandler,
  streamInfo
};