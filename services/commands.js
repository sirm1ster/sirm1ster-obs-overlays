const fs = require('fs');
const path = require('path');

const config = require("../configuration/config.json");
const { debug, log, operator, timestamp, modifier, fetchFile, convertCSVToArray } = require('./utils');
const socket = require("./socket");
const { saveGame } = require("./game");
const { addSingleQuote, getSingleQuote } = require("./quotes");
const { addSingleLegoBuild } = require('./lego');

const counter = (type, gameConfig, modifier = '+1') => {
  debug(`=== COUNTER ===`);
  debug(`Updating COUNTER of type '${type}'`);
  debug(`Updating COUNTER with modifier '${modifier}'`);

  debug(`gameConfig: ${JSON.stringify(gameConfig)}`);

  debug(`Modifying count using '${modifier}'`);

  gameConfig.counters[type].count = operator[modifier[0]](
    Number(gameConfig.counters[type].count),
    Number(modifier.substr(1))
  );

  if(gameConfig.counters[type].count > gameConfig.counters[type].max) {
    debug(`Updated count (${gameConfig.counters[type].count}) has exceed the maximum (${gameConfig.counters[type].max})`);
    return false;
  }

  log(`Counter '${type}' updated to ${gameConfig.counters[type].count}`);

  debug(`Updated gameConfig: ${JSON.stringify(gameConfig)}`);

  if(saveGame(gameConfig)) {
    socket.send(
      JSON.stringify({
        type: 'counter',
        data: gameConfig,
      })
    );
    gameConfig.meta = {
      "modifier": modifier[0]
    };
    return gameConfig;
  }

};

const quotes = (chatMessageObject, game, type = "") => {
  // if(!config.twitch.commands[args[0]].options[args[1]]) return;

  const quoteText = chatMessageObject.message;
  let commandResponse = "";

  if(chatMessageObject.modifier === 'add' && chatMessageObject.command === '?dadjoke') {
    commandResponse = addSingleQuote(quoteText, game, type);

    commandResponse.meta = {
      "modifier": chatMessageObject.modifier
    };

    debug(`commandResponse: ${JSON.stringify(commandResponse)}`);

    return commandResponse;
  }

  commandResponse = getSingleQuote(type);

  debug(`commandResponse: ${JSON.stringify(commandResponse)}`);

  return commandResponse;
}

const legos = (chatMessageObject) => {
  debug(`=== LEGOS ===`);

  if(chatMessageObject.modifier === 'add' && chatMessageObject.command === '?lego') {
    commandResponse = addSingleLegoBuild(chatMessageObject.message, type);
  }
}

const status = (hostStatus) => {
  debug(`=== STATUS ===`);

  const overlayState = {
    type: "overlay",
    data: {
      element: "status",
      value: ""
    }
  }
  let commandResponse = '';

  switch (hostStatus) {
    case 'brb':
      overlayState.data.value = "brb";
      break;

    default:
      break;
  }

  socket.send( JSON.stringify(overlayState) );
  commandResponse = overlayState.data;

  return commandResponse;
}

module.exports = {
  counter: counter,
  quotes: quotes,
  legos: legos,
  status: status
}