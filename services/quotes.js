const fs = require('fs');
const path = require('path');

const config = require("../configuration/config.json");
const { timestamp, fetchFile, saveFile, jsonOrderBy, modifier, randomNumber, filterByType, debug, log } = require("./utils");

const getAllQuotes = (type = "") => {
  debug(`>>> GETALLQUOTES`);
  const fileJSON = fetchFile('quotes');
  const filteredResponse = filterByType(fileJSON, type);

  try {
    log(`[${timestamp()}] Reading data from 'quotes.json'.`);
    return filteredResponse;
  } catch (err) {
    console.error(err);
    return false;
  }
}

const addSingleQuote = (text, game, type = "") => {
  const quotesStore = getAllQuotes(type);
  let dataQuotes = [];
  if(!!quotesStore) dataQuotes = jsonOrderBy('id', getAllQuotes(type));
  const lastQuoteItem = dataQuotes.slice(-1)[0];

  const quoteNew = {
    "id": (lastQuoteItem ? lastQuoteItem.id + 1 : 0),
    "text": text,
    "category": game,
    "timestamp": Date.now(),
    "type": type
  }

  const updatedQuoteStore = modifier.add(quoteNew, dataQuotes);
  const saveStatus = saveFile('quotes', updatedQuoteStore);

  log(`[${timestamp()}] New quote #${quoteNew.id} saved.`);

  if(saveStatus) {
    return quoteNew;
  }
}

const getSingleQuote = (type = '') => {
  const quotesStore = getAllQuotes(type);

  if(!quotesStore.length) return false;

  return quotesStore[randomNumber(quotesStore.length)];
}

module.exports = {
  getAllQuotes: getAllQuotes,
  addSingleQuote: addSingleQuote,
  getSingleQuote: getSingleQuote
}