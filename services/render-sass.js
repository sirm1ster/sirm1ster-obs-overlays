const sass = require('sass');
const fs = require('fs');
const path = require('path');

const renderSass = () => {
  const result = sass.renderSync({
    file: path.join(process.cwd(), 'public', 'sass','main.scss'),
    sourceMap: true,
    outputStyle: "compressed",
    outFile: path.join(process.cwd(), 'public', 'styles', 'styles.css')
  });

  const sassOutput = [{
    data: result.css.toString(),
    type: 'css'
  }, {
    data: result.map.toString(),
    type: 'map'
  }]

  for (let i = 0; i < sassOutput.length; i++) {
    const output = sassOutput[i];
    const fileExt = (output.type === 'css' ? 'css' : 'css.map')
    fs.writeFile(path.join('public','styles',`styles.${fileExt}`), output.data, (err) => {
      if(err) {
        console.log(err);
      }
    });
  }

}

module.exports = renderSass;