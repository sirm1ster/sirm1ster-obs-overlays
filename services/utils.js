const fs = require('fs');
const path = require('path');

const config = require("../configuration/config.json");

const operator = {
  '-': (a, b) => { return a - b},
  '+': (a, b) => { return a + b},
  '*': (a, b) => { return a * b},
  '/': (a, b) => { return a / b}
}

// add = add object A to array B
// remove = remove object A from array B
const modifier = {
  'add': (a, b) => {
    // a: object
    // b: object[]
    b.push(a);
    return b;
  },
  'remove': (a, b) => {
    // a: object
    // b: object[]
    for (let i = 0; i < b.length; i++) {
      const quote = b[i];
      if(b.indexOf(quote.id) !== -1) {
        b.splice(i, 1);
      }
    }
  }
}

const jsonOrderBy = (key, array) => {
  // key: string
  // array: object[]
  array.sort((a, b) => {
    const keyA = a[key];
    const keyB = b[key];
    if(keyA < keyB) return -1;
    if(keyA > keyB) return 1;
    return 0;
  });
  return array;
}

const filterByType = (array, value) => {
  console.log(`>>> FILTERBYTYPE`);

  if(!array) return false;

  let filteredItems = [];
  filteredItems = array.filter(item => {
    return item.type === value;
  });
  console.log(`filteredItems: ${JSON.stringify(filteredItems)}`)
  return filteredItems;
}

const arrayContainsValue = (allowedArray, givenArray) => {
  for(let i = 0; i < givenArray.length; i++) {
    return allowedArray.includes(givenArray[i]);
  }
}

const convertCSVToArray = (data) => {
  return data.split(',');
}

const fetchFile = (fileName) => {
  const filePath = path.join(process.cwd(), config.app.dataStore, `${fileName}.json`);
  const rawFileData = fs.readFileSync(filePath, 'utf8');

  if(rawFileData.length === 0) return false;

  return JSON.parse(rawFileData);
}

const saveFile = (fileName, data) => {
  const filePath = path.join(process.cwd(), config.app.dataStore, `${fileName}.json`);

  debug(`Saving to file '${filePath}'`);

  fs.writeFile(filePath, JSON.stringify(data), err => {
    if(err) {
      console.log(err);
      return err;
    }
  });

  return true;
}

const timestamp = () => {
  return new Date().toLocaleString();
}

const chatTimestamp = (epochStamp) => {
  const utcDate = new Date(epochStamp);
  const year = utcDate.getFullYear();
  const month = utcDate.getMonth() + 1;
  const date = utcDate.getDate();

  return `${date}/${month}/${year}`;
}

const randomNumber = (size) => {
  return Math.floor(Math.random() * size);
}

const isModerator = (msgObject, user) => {
  const currentCommand = config.twitch.commands[msgObject.command];
  const userBadges = Object.keys(user.badges);

  const userBadgeIsAllowed = arrayContainsValue(currentCommand.allowed, userBadges);

  if(msgObject.moderator && userBadgeIsAllowed) {
    return true;
  }

  return false;
}

const convertChatMessage = (chatMessage) => {
  const rawMessage = chatMessage.trim();

  debug(`Digesting raw message: '${JSON.stringify(rawMessage)}'`);

  const msgArgs = rawMessage.split(" ");

  let msgObject = {
    moderator: (msgArgs[0][0] === '?'),
    command: msgArgs[0],
    modifier: (msgArgs[0][0] === '?' ? msgArgs[1] : ''),
    message: (msgArgs[0][0] === '?' ? msgArgs.slice(2).join(" ") : msgArgs.slice(1).join(" "))
  }

  debug(`Converted message into object: ${JSON.stringify(msgObject)}`);

  return msgObject;
}

const validChatCommand = (cmd) => {
  debug(`Verifying command '${cmd}'`);
  if(cmd[0] === '?' || cmd[0] === '!') {
    debug(`Command '${cmd}' is a valid command.`);
    return true;
  }
  debug(`Command '${cmd}' is not a valid command.`);
  return false;
}

const debug = (debugMsg) => {
  if(process.env.APPMODE === 'dev') {
    console.log(`>>> ${debugMsg}`);
  }
}

const log = (logMsg) => {
  console.log(`[${timestamp()}] ${logMsg}`);
}

const escapeRegExp = (stringToGoIntoTheRegex) => {
  return stringToGoIntoTheRegex.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

const truncate = (str, n, useWordBoundary) => {
  if (str.length <= n) { return str; }
  const subString = str.substr(0, n-1); // the original check
  return (useWordBoundary
    ? subString.substr(0, subString.lastIndexOf(" "))
    : subString) + "&hellip;";
}

const camelize = (text) => {
  text = text.replace(/[-_\s.]+(.)?/g, (_, c) => c ? c.toUpperCase() : '');
  return text.substr(0, 1).toLowerCase() + text.substr(1);
}

module.exports = {
  debug,
  log,
  operator,
  modifier,
  timestamp,
  fetchFile,
  saveFile,
  chatTimestamp,
  randomNumber,
  jsonOrderBy,
  isModerator,
  filterByType,
  convertCSVToArray,
  convertChatMessage,
  validChatCommand,
  escapeRegExp,
  truncate,
  camelize
}