const config = require("../configuration/config.json");
const { debug } = require('./utils');

const messages = (type, data, gameData, modifier = '') => {
  switch (type) {
    case 'rip':
      switch(modifier) {
        case '+':
          return `${config.twitch.display_name} has died again.. NotLikeThis `;
        case '-':
          return `Let's revive ${config.twitch.display_name} a little bit`;
      }
    case 'korok':
      debug(`data: ${JSON.stringify(gameData)}`);
      switch(modifier) {
        case '+':
          return `${config.twitch.display_name} found another Korok Seed! PogChamp`;
        case '-':
          return `Whoops! There was a glitch in the Korok system! WutFace`;
      }
    case 'dadjoke':
      debug(`data: ${JSON.stringify(gameData)}`);
      switch(modifier) {
        case 'add':
          return `Successfully added Dad Joke #${data.id + 1}: "${data.text}" [${data.timestamp}]`;
        case '':
          if(!(data.id + 1)) {
            return `No Dad Jokes found... At least no good ones LUL`;
          }
          return `Dad Joke #${data.id + 1}: "${data.text}" [${data.timestamp}]`;
      }
    case 'status':
      debug(`data: ${JSON.stringify(gameData)}`);
      switch(modifier) {
        case 'brb':
          return `${config.twitch.display_name} will be right back. Stick around! TPFufun`;
        case '':
          return '';
      }
    case 'crew':
      debug(`gameData: ${JSON.stringify(gameData)}`);
      debug(`modifier: ${JSON.stringify(modifier)}`);
      switch(modifier) {
        case '-stock':
        case '+stock':
          return `Stock count updated`;
        case 'next':
        case 'prev':
          return `Player updated.`;
        case '-score':
        case '+score':
          return `Score updated.`;
        case '':
          return 'Updated.';
      }
    case 'cheer':
      debug(`type: ${JSON.stringify(type)}`);
      debug(`data: ${JSON.stringify(data)}`);
      return `What the frick!! ${data.user} has cheered ${data.bits} bits: "${data.message}" Kreygasm`;
    default:
      break;
  }
}

module.exports = messages;