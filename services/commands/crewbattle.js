const socket = require("../socket");
const { debug, fetchFile, operator, saveFile } = require("../utils");

const crewbattle = (chatMessageObject) => {

  debug(`chatMessageObject: ${JSON.stringify(chatMessageObject)}`);

  if(chatMessageObject.modifier == 'init') {
    const battleConfig = fetchFile('battle');
    debug(`battleConfig: ${JSON.stringify(battleConfig)}`);

    const initStatus = battleConfig;
    debug(`initStatus: ${JSON.stringify(initStatus)}`);

    if(initStatus) {
      socket.send(
        JSON.stringify({
          type: 'crewbattle',
          data: _battleGameViewData(initStatus),
        })
      );
      return initStatus;
    }
  }
}

const crewStocks = (chatMssgObject, command, modifier) => {
  debug(`=== CREWSTOCKS ===`);
  debug(`chatMssgObject: ${JSON.stringify(chatMssgObject)}`);
  debug(`Updating ${command} with modifier ${modifier}`);

  const crewUpdatedStatus = _updateCrewStocksScore(command.substring(1), modifier);

  debug(`crewUpdatedStatus: ${JSON.stringify(crewUpdatedStatus)}`);

  if(saveFile('battle', crewUpdatedStatus)) {
    socket.send(
      JSON.stringify({
        type: 'crewbattle',
        data: _battleGameViewData(crewUpdatedStatus),
      })
    );
    crewUpdatedStatus.meta = {
      "modifier": modifier
    };
    return crewUpdatedStatus;
  }
}

const crewPlayerChange = (chatMssgObject, command, modifier) => {
  debug(`=== CREWNEXT ===`);
  debug(`chatMssgObject: ${JSON.stringify(chatMssgObject)}`);
  debug(`Updating ${command} with modifier ${modifier}`);

  const crewNextPlayerUpdatedStatus = _changeCrewCurrentPlayer(command.substring(1), modifier);

  debug(`crewNextPlayerUpdatedStatus: ${JSON.stringify(crewNextPlayerUpdatedStatus)}`);

  if(saveFile('battle', crewNextPlayerUpdatedStatus)) {
    socket.send(
      JSON.stringify({
        type: 'crewbattle',
        data: _battleGameViewData(crewNextPlayerUpdatedStatus),
      })
    );
    crewNextPlayerUpdatedStatus.meta = {
      "modifier": modifier
    };
    return crewNextPlayerUpdatedStatus;
  }
}

/** PRIVATE METHODS */

const _changeCrewCurrentPlayer = (crewID, action) => {
  let battleConfig = fetchFile('battle');
  let storeCrewState = battleConfig.crews[crewID];

  for (let i = 0; i < storeCrewState.players.length; i++) {
    debug(`CurrentPlayer INDEX: ${i}`);

    if(storeCrewState.players[i].active === true) {
      storeCrewState = _setNextPrevPlayer(i, storeCrewState, action);

      battleConfig.crews[crewID].players = storeCrewState.players;

      return battleConfig;
    }

    debug(`storeCrewState.players: ${JSON.stringify(storeCrewState.players)}`);
    debug(`storeCrewState.players[i + 1]: ${JSON.stringify(storeCrewState.players[i + 1])}`);
  }

  battleConfig.crews[crewID].players = storeCrewState.players;

  return battleConfig;
}

const _setNextPrevPlayer = (index, storeCrewState, action) => {
  if(action === 'next' && index !== (storeCrewState.players.length - 1)) {
    storeCrewState.players[index].active = false;
    storeCrewState.players[index + 1].active = true;
    return storeCrewState;
  } else if(action === 'prev' && index !== 0) {
    storeCrewState.players[index].active = false;
    storeCrewState.players[index - 1].active = true;
    return storeCrewState;
  } else {
    return storeCrewState;
  }
}

const _updateCrewStocksScore = (crewID, modifier) => {
  let battleConfig = fetchFile('battle');

  const storeCrewState = battleConfig.crews[crewID];

  let updatedCrewState;
  if(modifier.substring(1) === 'stock') {
    updatedCrewState = _updateCurrentPlayer(storeCrewState, modifier);
    updatedCrewState.stocks = operator[modifier[0]](
      Number(updatedCrewState.stocks),
      Number(1)
    );
  } else if (modifier.substring(1) === 'score') {
    updatedCrewState = _updateCurrentScore(storeCrewState, modifier);
  }

  battleConfig.crews[crewID] = updatedCrewState;

  return battleConfig;
}

const _updateCurrentPlayer = (storeCrewState, modifier) => {
  const givenCrewState = storeCrewState;
  const currentPlayer = _fetchCurrentPlayer(storeCrewState);
  debug(`currentPlayer: ${JSON.stringify(currentPlayer)}`);

  currentPlayer.stocks = operator[modifier[0]](
    Number(currentPlayer.stocks),
    Number(1)
  )

  for (let i = 0; i < givenCrewState.players.length; i++) {
    if(givenCrewState.players[i].name === currentPlayer.name) {
      givenCrewState.players[i] = currentPlayer;
    }
  }
  debug(`givenCrewState: ${JSON.stringify(givenCrewState)}`);

  return givenCrewState;
}

const _updateCurrentScore = (storeCrewState, modifier) => {
  const givenCrewState = storeCrewState;

  givenCrewState.score = operator[modifier[0]](
    Number(givenCrewState.score),
    Number(1)
  );

  debug(`givenCrewState: ${JSON.stringify(givenCrewState)}`);

  return givenCrewState;
}

const _battleGameViewData = (data) => {
  const storeData = data;
  for (const key in storeData.crews) {
    if (Object.hasOwnProperty.call(storeData.crews, key)) {
      const crew = storeData.crews[key];
      const purgedCrew = crew.players.filter(player => player.active);
      crew.players = purgedCrew[0];
    }
  }
  return storeData;
}

const _fetchCurrentPlayer = (crewData) => {
  const purgedCrew = crewData.players.filter(player => player.active);
  debug(`purgedCrew: ${JSON.stringify(purgedCrew[0])}`);

  return purgedCrew[0];
}


module.exports = {
  crewbattle: crewbattle,
  crewStocks: crewStocks,
  crewPlayerChange: crewPlayerChange
}