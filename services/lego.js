const { debug, log, operator, timestamp, modifier, fetchFile, convertCSVToArray } = require('./utils');

const addSingleLegoBuild = (message, type) => {
  debug(`=== addSingleLegoBuild ===`);

  const legoProperties = convertCSVToArray(message);

  debug(`legoProperties: ${JSON.stringify(legoProperties)}`);
}

module.exports = {
  addSingleLegoBuild: addSingleLegoBuild
}