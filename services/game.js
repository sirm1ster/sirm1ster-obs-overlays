const fs = require('fs');
const path = require('path');

const config = require("../configuration/config.json");
const { debug, saveFile, fetchFile } = require("./utils");

const getGameConfig = (identifierNr, type = 'games') => {
  try {
    debug(`Reading data from '${type}.json'.`);

    const fileData = fetchFile(type);

    return fileData[identifierNr];

  } catch (err) {
    console.error(err);
    return;
  }
}

const saveGame = (saveGame) => {
  debug(`Saving game with ID '${saveGame.game_id}'`);

  const gamesStore = fetchFile('games');

  gamesStore[saveGame.game_id] = saveGame;

  const saveGameStatus = saveFile('games', gamesStore);

  return saveGameStatus;
}

module.exports = {
  saveGame: saveGame,
  getGameConfig: getGameConfig
};